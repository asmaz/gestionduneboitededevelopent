const express=require("express")
const bodyparser=require('body-parser')
const User=require('./routers/user_router')
const Master=require('./routers/master_router')
const Chef=require('./routers/chef_router')
const Member=require('./routers/member_router')
const Equipe=require('./routers/equipe_router')
const Discussion=require('./routers/discussion_router')
const Projet=require('./routers/projet_router')



const db=require('./models/db.js');
const app= express();




app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json());

app.use('/user ', User)
app.use('/master',Master)
app.use('/chef',Chef)
app.use('/member',Member)
app.use('/equipe',Equipe)
app.use('/discussion',Discussion)
app.use('/projet',Projet)


app.listen(4000,function(){
    console.log('bonjour')
})