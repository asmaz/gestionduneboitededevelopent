const chefModel =require('../models/chef_projet')
const fs=require("fs");
const multer=require("multer");
const upload=multer({dest:__dirname+"/uploads/images"})
const bcrypt= require('bcryptjs');

module.exports={

addchef:function(req,res){

    var file=__dirname+"/uploads/images"+req.file.originalname
    fs.readFile(req.file.path,function(err,data){//read bech ta9ra image 
        fs.writeFile(file,data,function(err){//write tsob image fi base 
            if(err)
            {
                res.json({state:'no',msg:'vous avez un erreur'})
            } 
            else{
                              const chef =new chefModel({
                                nom:req.body.nom,
                                statue:req.body.statue,
                                email:req.body.email,
                                password:req.body.password,
                                specialite:req.body.specialite,
                                education:req.body.education,
                                image:req.file.originalname,
                                pseudo:req.body.pseudo,
                                projet:req.body.projet
                              })

                              chef.save(function(err){
                                    if(err){
                                      res.json({state:'no',msg:'erreur'})
                                  
                                    }else{
                                       res.json({state:'ok',msg:'chef ajouté'})
                                     
                                      }
                              })
                            }
                        })
                    })
               
                },
uploadimage:function(req,res){
var file=__dirname+"/uploads/images/"+req.file.originalname
fs.readFile(req.file.path,function(err,data){
 fs.writeFile(file,data,function(err){
     if(err)
            {
                var response={
                    message:'sorry file couldnt upload',
                    filename:req.file.originalname,
                }
            }
            else{
                res.json({state:'ok',msg:'ok '})
            }
    
            })
        })
    },
getfile:function(req,res){
        res.sendFile(__dirname+"/uploads/images"+req.params.image)
    },
    
getAllchef:function(req,res){
        
        chefModel.find({},function(err,data){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(data)
            }
        })
    },
getbyIdchef:function(req,res){
        chefModel.findOne({_id:req.params.id}, function(err,data){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(data)
            }
        })
    },
deletechef:function(req,res){
        chefModel.deleteOne({_id:req.params.id},function(err,data){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state:'ok',msg:'chef supprimer'})
            }
        })
        
    },
update:function(req,res){
        chefModel.updateOne({_id:req.params.id},{$set:req.body},
            {
                nom:req.body.nom,
                statue:req.body.statue,
                email:req.body.email,
                password:req.body.password,
                specialite:req.body.specialite,
                education:req.body.education,
                image:req.file.originalname,
                pseudo:req.body.pseudo,
            },
            function(err){
                if(err){
                    res.json({state:'no',msg:'vous avez un erreur'})
                }
                else{
                    res.json({state:'ok',msg:'chef modifier'})
                }
            } ) },

           
            
        }
        

























