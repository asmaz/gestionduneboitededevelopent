const projetmodel=require('../models/projet_model');
module.exports={   
     addprojet:function(req,res){
    const projet = new projetmodel({
       
        theme:req.body.theme,
        date_deb:req.body.date_deb,
        date_fin:req.body.date_fin,
        master:req.body.master,
        equipe:req.body.equipe,
        chef:req.body.chef
            });
    projet.save(function(err){
        if(err){
            res.json({state:'no',msg:'vous avez un erreur'})
            console.log(err)
        }
        else{
            res.json({state:'ok',msg:'projet ajouter'})
        }

    } )
},
getallprojet:function(req,res){
     projetmodel.find({}).populate("master chef equipe") .exec(function(err,data){
        if(err){
            res.json({state:'no',msg:'vous avez un erreur'})
        }
        else{
            res.json(data)
        }
    })
},
getbyId:function(req,res){
    projetmodel.findOne({_id:req.params.id}, function(err,data){
        if(err){
            res.json({state:'no',msg:'vous avez un erreur'})
        }
        else{
            res.json(data)
        }
    })
},
deleteprojet:function(req,res){
    projetmodel.deleteOne({_id:req.params.id},function(err,data){
        if(err){
            res.json({state:'no',msg:'vous avez un erreur'})
        }
        else{
            res.json({state:'ok',msg:'projet supprimer'})
        }
    })
    
},
update:function(req,res){
    projetmodel.updateOne({_id:req.params.id},{$set:req.body},
        {
            theme:req.body.theme,
            date_deb:req.body.date_deb,
            date_fin:req.body.date_fin
           
        },
        function(err){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state:'ok',msg:'projet modifier'})
            }
    } ) },
pushprojet:function(req,res) {
        projetmodel.updateOne({_id:req.params.id},{$push:{master:req.body.master}},{$push:{member:req.body.member}},
            function(err,data) {
                if(err){
                    res.json({state:'no',msg:'vous avez un err'})
                }
                else{
                    res.json(data)
        }
  } )
     }


}
