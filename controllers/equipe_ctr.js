const equipemodel=require('../models/equipe_model');
module.exports={
    addequipe:function(req,res){
        const equipe = new equipemodel({
            member:req.body.member,
            master:req.body.master,
        });
        equipe.save(function(err){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state:'ok',msg:'equipe ajouter'})
            }

        } )
    },
    getallequipe:function(req,res){
    

         equipemodel.find({}).populate('member master').exec(function(err,data){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
             
            }
            else{
                res.json(data)
                console.log(data)
            }
        })
    },
    getbyId:function(req,res){
        equipemodel.findOne({_id:req.params.id}, function(err,data){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(data)
            }
        })
    },
    deleteequipe:function(req,res){
        equipemodel.deleteOne({_id:req.params.id},function(err,data){
            if(err){
                res.json({state:'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state:'ok',msg:'equipe supprimer'})
            }
        })
        
    },
    update:function(req,res){
        equipemodel.updateOne({_id:req.params.id},{$set:req.body},
            {
               
            },
            function(err){
                if(err){
                    res.json({state:'no',msg:'vous avez un erreur'})
                }
                else{
                    res.json({state:'ok',msg:'equipe modifier'})
                }
        } ) },
    pushequipe:function(req,res) {
            equipemodel.updateOne({_id:req.params.id},{$push:{master:req.body.master}},{$push:{member:req.body.member}},
                function(err,data) {
                    if(err){
                        res.json({state:'no',msg:'vous avez un err'})
                    }
                    else{
                        res.json(data)
            }
      } )
         }
}
