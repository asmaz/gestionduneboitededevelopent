const projetcontroller = require('../controllers/projet_ctr');
const router=require('express').Router();

router.post("/addprojet", projetcontroller.addprojet)
router.get("/afficherprojet", projetcontroller.getallprojet)
router.get("/afficheroneprojet/:id", projetcontroller.getbyId)
router.delete("/supprimerprojet/:id", projetcontroller.deleteprojet)
router.put("/modifier/:id", projetcontroller.update)
router.put("/push/:id",projetcontroller.pushprojet)

module.exports=router;