const equipecontroller = require('../controllers/equipe_ctr');
const router=require('express').Router();
router.post("/addequipe",equipecontroller.addequipe)
router.get("/afficherequipe", equipecontroller.getallequipe)
router.get("/afficherone/:id", equipecontroller.getbyId)
router.delete("/supprimerequipe/:id", equipecontroller.deleteequipe)
router.put("/modifier/:id", equipecontroller.update)
router.put("/push/:id", equipecontroller.pushequipe)
module.exports=router;