const membercontroller = require('../controllers/membre_ctr');
const router=require('express').Router();
const multer=require("multer")
const upload=multer({dest:__dirname+"/uploads/images"})
router.post("/addmember", upload.single("image"),membercontroller.addmemmber)
router.get("/affichermember", membercontroller.getAllmember)
router.get("/affichemember/:id", membercontroller.getbyIdmember)
router.delete("/supprimermember/:id", membercontroller.deletemember)
router.put("/modifier/:id", membercontroller.update)
router.post("/uploadimage", upload.single("image"),membercontroller.uploadimage)
router.get("/getfile/:image", membercontroller.getfile)
module.exports=router;