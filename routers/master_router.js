const mastercontroller = require('../controllers/master_ctr');
const router=require('express').Router();
const multer=require("multer")
const upload=multer({dest:__dirname+"/uploads/images"})
router.post("/addmaster", upload.single("image"),mastercontroller.addmaster)
router.get("/affichermaster", mastercontroller.getAllmaster)
router.get("/afficherone/:id", mastercontroller.getbyIdmaster)
router.delete("/supprimermaster/:id", mastercontroller.deletemaster)
router.put("/modifier/:id", mastercontroller.update)
router.post("/uploadimage", upload.single("image"),mastercontroller.uploadimage)
router.get("/getfile/:image", mastercontroller.getfile)

module.exports=router;