const usercontroller = require('../controllers/user_controller');
const router=require('express').Router();
const multer=require("multer")
const upload=multer({dest:__dirname+"/uploads/images"})
router.post("/adduser", upload.single("image"),usercontroller.addUser)
router.get("/afficheruser", usercontroller.getAllUser)
router.get("/afficherone/:id", usercontroller.getbyIdUser)
router.delete("/supprimeruser/:id", usercontroller.deleteUser)
router.put("/modifier/:id", usercontroller.update)
router.post("/uploadimage", upload.single("image"),usercontroller.uploadimage)
router.get("/getfile/:image", usercontroller.getfile)
module.exports=router;