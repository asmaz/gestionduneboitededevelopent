const chefcontroller = require('../controllers/chef_ctr');
const router=require('express').Router();
const multer=require("multer")
const upload=multer({dest:__dirname+"/uploads/images"})
router.post("/addchef", upload.single("image"),chefcontroller.addchef)
router.get("/afficherchef", chefcontroller.getAllchef)
router.get("/afficherone/:id", chefcontroller.getbyIdchef)
router.delete("/supprimerchef/:id", chefcontroller.deletechef)
router.put("/modifier/:id", chefcontroller.update)
router.post("/uploadimage", upload.single("image"),chefcontroller.uploadimage)
router.get("/getfile/:image", chefcontroller.getfile)

module.exports=router;