const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const equipeschema = mongoose.model('equipe', new mongoose.Schema({
    member:[{
        type:Schema.Types.ObjectId,
        ref:'member'  
    }],
    master:[{
        type:Schema.Types.ObjectId,
        ref:'master'  
    }],
    projet:[{
        type:Schema.Types.ObjectId,
        ref:'projet'  
    }],
   
 
})

)
module.exports= equipeschema ;