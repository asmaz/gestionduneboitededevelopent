const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const discussschema = mongoose.model('discussion', new mongoose.Schema({
  
 emetteur:{
 type:int,
 trim:true,
 required:true
 },
 recepteur:{
    type:int,
    trim:true,
    required:true
 },
 message:{
    type:int,
    trim:true,
    required:true
 }
})

)
module.exports= discussschema ;