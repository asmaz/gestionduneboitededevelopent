const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const projetschema = mongoose.model('projet', new mongoose.Schema({

    theme:{
      type:String,
      trim:true,
      required:true
    },
    date_deb:{
      type:Date,
      trim:true,
      required:true
    },

    date_fin:{
        type:Date,
        trim:true,
        required:true
    },
    
    master:[{
        type:Schema.Types.ObjectId,
        ref:'master'  
    }],
    equipe:[{
        type:Schema.Types.ObjectId,
        ref:'equipe'  
    }],
    chef:[{
        type:Schema.Types.ObjectId,
        ref:'chef'  
    }]
})

)
module.exports= projetschema ;