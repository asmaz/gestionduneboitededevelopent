const mongoose = require('mongoose');
const bcrypt= require('bcryptjs');

const Schema = mongoose.Schema;


const baseOption = {
    discriminatorKey:"usertype",
    collection:"user",
   
}
const userschema = mongoose.model('user', new mongoose.Schema({
    nom:{
        type: String,
        required: true,
        trim: true,
    },
  
    statue:{
        type: String,
        required: true,
        trim: true,

    },
    email:{
        type: String,
        required: true,
        trim: true,

    },

    password:{
        type: String,
        required: true,
        trim: true,

    },
  
    specialite:{
        type: String,
        required: true,
        trim: true,

    },
    education:{
        type: String,
        required: true,
        trim: true,

    },
    pseudo:{
        type: String,
        required: true,

    },
    image:{
      type:String,
      required:true,
    },
    Comment:{
        type : mongoose.Schema.Types.ObjectID,
        ref : 'comm'
    }
},baseOption)
.pre("save", function(){
    this.password=bcrypt.hashSync(this.password,10)
})
)
module.exports=userschema;
